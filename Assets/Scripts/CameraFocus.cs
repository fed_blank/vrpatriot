﻿using Vuforia;
using UnityEngine;
using System.Collections;

/// <summary>
/// Solution to fix problem with camera focus error.
/// </summary>
public class CameraFocus : MonoBehaviour
{
    #region Methods

    #region Private Methods

    private void Start()
    {
        StartCoroutine(WaitVuforia());
    }

    private IEnumerator WaitVuforia()
    {
        while (!VuforiaARController.Instance.HasStarted)
        {
            yield return null;
        }

        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    /// <summary>
    /// When our application was resumed or catch focus
    /// </summary>
    private void AfterPause()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    /// <summary>
    /// When our application lose focus or hiden
    /// </summary>
    private void OnPause()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
    }

    #endregion

    #region Unity Methods

    private void OnEnable()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            AfterPause();
        }

        else
        {
            OnPause();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            AfterPause();
        }

        else
        {
            OnPause();
        }
    }

    #endregion

    #endregion
}