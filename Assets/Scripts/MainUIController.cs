﻿using UnityEngine;
using UnityEngine.UI;

public class MainUIController : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private Button _quitButton;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _quitButton.onClick.AddListener(() =>
        {
            SceneLoader.QuitApp();
        });

    }

    #endregion
}
