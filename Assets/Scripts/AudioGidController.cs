﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AudioGidController : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private Button _playAudioButton;
    [SerializeField] private Button _pauseAudioButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private TMP_Text _timeText;

    #endregion

    #region Private Fields

    private bool _isPlayed = false;

    #endregion

    #region Unity Methods

    void Awake()
    {
        _playAudioButton.onClick.AddListener(() =>
        {
            _audioSource.Play();
            _isPlayed = true;
            _playAudioButton.gameObject.SetActive(false);
            _pauseAudioButton.gameObject.SetActive(true);
            BackgroundMusic.Instance.DecreaseVolumeWhenPlayOther();
        });

        _pauseAudioButton.onClick.AddListener(() =>
        {
            _audioSource.Pause();
            _playAudioButton.gameObject.SetActive(true);
            _pauseAudioButton.gameObject.SetActive(false);
            BackgroundMusic.Instance.SetBaseVolume();
        });

        _restartButton.onClick.AddListener(() =>
        {
            _audioSource.time = 0;
            SetTimeText();
        });

        _audioSource.volume = 1;
    }

    private void Start()
    {
        SetTimeText();
    }

    private void Update()
    {
        if (_audioSource.isPlaying)
        {
            SetTimeText();
        }
        else
        {
            if (_isPlayed)
            {
                Debug.Log("Конец проигрыша");
                SetBaseParametrs();
                _isPlayed = false;
            }
        }
    }

    #endregion


    #region Private Methods

    /// <summary>
    /// отображение состояния времени аудио клипа в канвасе
    /// </summary>
    private void SetTimeText()
    {
        float currentTime = _audioSource.time;
        string currentMinutes = (currentTime / 60).ToString("00");
        string currentSeconds = (currentTime % 60).ToString("00");
        string maxMinutes = (_audioSource.clip.length / 60).ToString("00");
        string maxSeconds = (_audioSource.clip.length % 60).ToString("00");

        _timeText.text = string.Format("{0}:{1}/{2}:{3}", currentMinutes, currentSeconds, maxMinutes, maxSeconds);
    }

    private void SetBaseParametrs()
    {
        _playAudioButton.gameObject.SetActive(true);
        _pauseAudioButton.gameObject.SetActive(false);
        BackgroundMusic.Instance.SetBaseVolume();
    }

    #endregion
}
