﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BackgroundMusic : MonoBehaviour // Singleton
{

    #region Constants

    private const float VOLUME_DECREASE_VALUE_FOR_GUID = 0.5f;

    #endregion

    #region Properties

    public static BackgroundMusic Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    #endregion

    #region Private Fields

    private static BackgroundMusic instance = null;
    private AudioSource _audioSource;
    private float _baseVolume;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _baseVolume = _audioSource.volume;
    }

    #endregion

    #region Public Methods

    public void DecreaseVolumeWhenPlayOther()
    {
        _audioSource.volume -= (VOLUME_DECREASE_VALUE_FOR_GUID <= _audioSource.volume) ? VOLUME_DECREASE_VALUE_FOR_GUID : _audioSource.volume;
    }

    public void SetBaseVolume()
    {
        _audioSource.volume = _baseVolume;
    }

    #endregion
}
