﻿using UnityEngine;

public class MouseOrbitCSHARP : MonoBehaviour
{
    #region Properties

    public Transform Target
    {
        get
        {
            return _target;
        }
        set
        {
            _target = value;
        }
    }

    #endregion

    #region Unity Editor

    [SerializeField] private Transform _target;
    [SerializeField] private float _distance = 5.0f;
    [SerializeField] private float _xSpeed = 250.0f;
    [SerializeField] private float _ySpeed = 120.0f;
    [SerializeField] private float _yMinLimit = -20.0f;
    [SerializeField] private float _yMaxLimit = 80.0f;

    #endregion

    #region Private Fields

    private float x;
    private float y;

    #endregion

    #region Unity Methods

    void Awake()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.x;
        y = angles.y;

        if (GetComponent<Rigidbody>() != null)
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
    }

    private void Start()
    {
        if (_target == null)
        {
            Debug.LogError("MouseOrbitCSHARP.cs <color=red>target is null</color>");
        }
        GenerateTransform();
    }

    void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            CameraMovement();
        }
    }

    #endregion

    #region Private Methods

    private void CameraMovement()
    {
        x += (float)(Input.GetAxis("Mouse X") * _xSpeed * 0.02f);
        y -= (float)(Input.GetAxis("Mouse Y") * _ySpeed * 0.02f);

        GenerateTransform();
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }

    #endregion

    #region Public Methods

    public void GenerateTransform()
    {
        y = ClampAngle(y, _yMinLimit, _yMaxLimit);
        Quaternion rotation = Quaternion.Euler(y, x, 0);
        Vector3 position = rotation * (Vector3.right * 0 + Vector3.up * 0 + Vector3.forward * (-_distance)) + _target.position;

        transform.rotation = rotation;
        transform.position = position;
    }

    #endregion
}
