﻿using UnityEngine;

public class CamSwitch : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private MouseOrbitCSHARP _cam;
    [SerializeField] private Transform _orbit1, _orbit2;

    #endregion

    #region Private Fields

    private bool _isOne = true;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _cam.Target = _orbit1;
    }

    #endregion

    #region Public Methods

    public void ChangeCam()
    {
        Debug.Log("change orbit");

        if (_isOne)
        {
            _cam.Target = _orbit2;
        }
        else
        {
            _cam.Target = _orbit1;
        }

        _isOne = !_isOne;

        _cam.GenerateTransform();
    }

    #endregion
}